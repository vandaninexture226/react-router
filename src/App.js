import React, { Component, useState } from "react";
import './App.css';
import { BrowserRouter as Router, Routes, Route, useParams, Link, NavLink, Navigate } from "react-router-dom";

const User = () => {
  let {username} = useParams();
  return (<h1> Welcome User {username}</h1>)
}
class App extends Component{
  state = {
    loggedIn:false
  }
  loginHandle = ()=>{
    this.setState(prevState => ({
      loggedIn: !prevState.loggedIn
    }))
  }

  render(){
    return (
        <Router>
          <div className="App">
            <ul>
              <li>
                <NavLink to="/" activeClassName="active">Home</NavLink>
              </li>
              <li>
                <NavLink to="/about" activeClassName="active">About</NavLink>
              </li>
              <li>
                <NavLink to="/user/vandan" activeClassName="active">User Vandan</NavLink>
              </li>
              <li>
                <NavLink to="/user/jay" activeClassName="active">User Jay</NavLink>
              </li>
            </ul>
            <input type="button" value={this.state.loggedIn ? 'log out' : 'log in'} onClick={this.loginHandle.bind(this)} />
          <Routes>
            <Route path="/" element={ <h1>Welcome Home</h1>} />
            <Route path="/about" element={ <h1>Welcome About</h1>} />
            <Route path="/user/:username" element={this.state.loggedIn ? (<User/>) : (<Navigate to="/" />) } />
          </Routes>
        </div>
      </Router>
    );
  }
}

export default App;
